package com.SDA.company.controller;

import com.SDA.company.component.CustomFaker;
import com.SDA.company.models.Company;
import com.SDA.company.service.CompanyService;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping("/company")
@ControllerAdvice
public class CompanyController {


    private final CompanyService companyService;

    @Autowired // -> This is not needed, it is here for learning purposes only
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @Autowired
    private CustomFaker customFaker;

    @PostMapping("/create")
    public ResponseEntity<Company> createCompany(@RequestBody Company company, Principal principal) {
        return ResponseEntity.ok(companyService.createCompany(company, principal.getName()));
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Company>> getAllCompanies() {
        return ResponseEntity.ok(companyService.getAllCompanies());
    }

    @GetMapping("/findById")
    public ResponseEntity<Company> findById(@RequestParam Integer companyId) {
        return ResponseEntity.ok(companyService.findById(companyId));
    }

    @GetMapping("/populate")
    public ResponseEntity<String> populateDB() {
        Integer numberOfGeneratedCompanies = companyService.createAll(customFaker.autogenerateCompanyList()).size();
        return ResponseEntity.ok("The list has been populated with " + numberOfGeneratedCompanies + " companies!");
    }

    @GetMapping("/getAllSorted")
    public ResponseEntity<List<Company>> getAllSortedCompanies(
            @RequestParam(defaultValue = "0") Integer pageNumber,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy) {
        return ResponseEntity.ok(companyService.getAllSortedCompanies(pageNumber, pageSize, sortBy));
    }


}
