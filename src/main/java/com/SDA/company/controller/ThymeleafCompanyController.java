package com.SDA.company.controller;

import com.SDA.company.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;

@Controller
public class ThymeleafCompanyController {

    private final CompanyService companyService;

    @Autowired
    public ThymeleafCompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }
}
