package com.SDA.company.service;

import com.SDA.company.models.Employee;

import java.util.*;

public interface EmployeeService {
    Employee createEmployee(Employee employee);

    List<Employee> createAll(List<Employee> employeeList);

    List<Employee> getAllEmployees();
    
    List<Employee> getAllSortedEmployees(Integer pageNumber, Integer pageSize, String sortBy);
}
