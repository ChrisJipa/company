package com.SDA.company.service;

import com.SDA.company.models.Company;

import java.util.*;

public interface CompanyService {
    Company createCompany(Company company, String userName);

    Company findById(Integer companyId);

    List<Company> createAll(List<Company> companies);

    List<Company> getAllCompanies();

    List<Company> getAllSortedCompanies(Integer pageNumber, Integer pageSize, String sortBy);
}
