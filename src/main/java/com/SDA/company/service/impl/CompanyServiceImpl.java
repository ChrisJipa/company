package com.SDA.company.service.impl;

import com.SDA.company.exceptions.CompanyNotFoundException;
import com.SDA.company.models.Company;
import com.SDA.company.repository.CompanyRepository;
import com.SDA.company.service.CompanyService;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;

    public CompanyServiceImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public Company createCompany(Company company, String userName) {
        company.setCreatedBy(userName);
        return companyRepository.save(company);
    }

    @Override
    public Company findById(Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        return company.orElseThrow(() -> new CompanyNotFoundException("Company with Id: " + companyId + " was not found!"));
    }

    @Override
    public List<Company> createAll(List<Company> companies) {
        return (List<Company>) companyRepository.saveAll(companies);
    }

    @Override
    public List<Company> getAllCompanies() {
        return (List<Company>) companyRepository.findAll();
    }

    @Override
    public List<Company> getAllSortedCompanies(Integer pageNumber, Integer pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy));
        Page<Company> pageResult = companyRepository.findAll(pageable);

        return pageResult.getContent();
    }
}
