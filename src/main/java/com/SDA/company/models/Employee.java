package com.SDA.company.models;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String email;

    @Column
    private String phoneNumber;

    @Column
    private Long personalNumericCode;

    //Setters
    public void setId (Integer id) {
        this.id = id;
    }

    public void setFirstName (String firstName) {
        this.firstName = firstName;
    }

    public void setLastName (String lastName) {
        this.lastName = lastName;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPersonalNumericCode (Long personalNumericCode) {
        this.personalNumericCode = personalNumericCode;
    }

    //Getters
    public Integer getId () {
        return this.id;
    }

    public String getFirstName () {
        return this.firstName;
    }

    public String getLastname () {
        return this.lastName;
    }

    public String getEmail () {
        return this.email;
    }

    public String getPhoneNumber () {
        return this.phoneNumber;
    }

    public Long getPersonalNumericCode () {
        return this.personalNumericCode;
    }
}
