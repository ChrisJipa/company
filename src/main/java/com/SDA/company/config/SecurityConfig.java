package com.SDA.company.config;

import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.authentication.builders.*;
import org.springframework.security.config.annotation.web.builders.*;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.crypto.bcrypt.*;
import org.springframework.security.crypto.password.*;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) {
        try {

            authenticationManagerBuilder.inMemoryAuthentication()
                    .withUser("admin").password(passwordEncoder().encode("1234")).roles("ADMIN", "USER")
                    .and()
                    .withUser("user").password(passwordEncoder().encode("1234")).roles("USER");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void configure(HttpSecurity httpSecurity) {
        try {

            httpSecurity.authorizeRequests()
//                    .antMatchers("/company/create").hasRole("ADMIN")
//                    .antMatchers("/company/getAll").hasAnyRole("USER", "ADMIN")
//                    .anyRequest().authenticated()
                    .anyRequest().permitAll() //--> Temporarily disable authentication
                    .and()
                    .httpBasic();

        }catch (Exception e) {
            e.printStackTrace();
        }

        try {
            httpSecurity.csrf().disable().authorizeRequests()
                    .and()
                    .cors().disable().authorizeRequests();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
